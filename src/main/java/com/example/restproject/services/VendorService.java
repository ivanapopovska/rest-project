package com.example.restproject.services;

import com.example.restproject.api.v1.model.VendorDTO;
import com.example.restproject.domain.Vendor;

import java.util.List;

public interface VendorService {
    List<VendorDTO> getAllVendors();
    VendorDTO getVendorById(Long id);
    VendorDTO saveVendor(VendorDTO vendorDTO);
    VendorDTO updateVendor(Long id, VendorDTO vendorDTO);
    void deleteVendor(Long id);
}
