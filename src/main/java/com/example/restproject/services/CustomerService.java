package com.example.restproject.services;

import com.example.restproject.api.v1.model.CustomerDTO;

import java.util.List;

public interface CustomerService {
    List<CustomerDTO> getAllCustomers();
    CustomerDTO getCustomerById(Long id);
    CustomerDTO saveCustomer(CustomerDTO customerDTO);
    CustomerDTO updateCustomer (Long id, CustomerDTO customerDTO);
    void deleteCustomer(Long id);
}
