package com.example.restproject.services;

import com.example.restproject.api.v1.mapper.VendorMapper;
import com.example.restproject.api.v1.model.VendorDTO;
import com.example.restproject.domain.Vendor;
import com.example.restproject.repositories.VendorRepo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class VendorServiceImpl implements VendorService{

    private final VendorRepo vendorRepo;
    private final VendorMapper vendorMapper;

    public VendorServiceImpl(VendorRepo vendorRepo, VendorMapper vendorMapper) {
        this.vendorRepo = vendorRepo;
        this.vendorMapper = vendorMapper;
    }

    @Override
    public List<VendorDTO> getAllVendors() {
        return vendorRepo.findAll()
                .stream()
                .map(vendorMapper::vendorToVendorDTO)
                .collect(Collectors.toList());
    }

    @Override
    public VendorDTO getVendorById(Long id) {
        Optional<Vendor>vendor = vendorRepo.findById(id);
        Vendor found = vendor.get();
        return vendorMapper.vendorToVendorDTO(found);
    }

    @Override
    public VendorDTO saveVendor(VendorDTO vendorDTO) {
        Vendor vendor = vendorMapper.vendorDTOToVendor(vendorDTO);
        Vendor saved = vendorRepo.save(vendor);
        return vendorMapper.vendorToVendorDTO(saved);
    }

    @Override
    public VendorDTO updateVendor(Long id, VendorDTO vendorDTO) {
        Vendor vendor = vendorMapper.vendorDTOToVendor(vendorDTO);
        vendor.setId(id);
        Vendor v = vendorRepo.save(vendor);
        return vendorMapper.vendorToVendorDTO(v);
    }

    @Override
    public void deleteVendor(Long id) {
        vendorRepo.deleteById(id);
    }
}
