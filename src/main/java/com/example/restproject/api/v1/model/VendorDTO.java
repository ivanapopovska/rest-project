package com.example.restproject.api.v1.model;

import lombok.Data;

@Data
public class VendorDTO {
    private Long id;
    private String name;
}
