package com.example.restproject.api.v1.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@AllArgsConstructor
@Data
public class CustomerListDTO {
    List<CustomerDTO> customers;
}
