package com.example.restproject.api.v1.mapper;

import com.example.restproject.api.v1.model.CategoryDTO;
import com.example.restproject.domain.Category;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CategoryMapper{

    CategoryMapper INSTANCE = Mappers.getMapper(CategoryMapper.class);


    CategoryDTO categoryToCategoryDTO (Category category);

}