package com.example.restproject.api.v1.mapper;

import com.example.restproject.api.v1.model.CustomerDTO;
import com.example.restproject.api.v1.model.VendorDTO;
import com.example.restproject.domain.Customer;
import com.example.restproject.domain.Vendor;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface VendorMapper {
    CustomerMapper INSTANCE = Mappers.getMapper(CustomerMapper.class);

    VendorDTO vendorToVendorDTO(Vendor vendor);
    Vendor vendorDTOToVendor (VendorDTO vendorDTO);
}
