package com.example.restproject.api.v1.mapper;

import com.example.restproject.api.v1.model.CategoryDTO;
import com.example.restproject.api.v1.model.CustomerDTO;
import com.example.restproject.domain.Category;
import com.example.restproject.domain.Customer;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CustomerMapper {

    CustomerMapper INSTANCE = Mappers.getMapper(CustomerMapper.class);

    CustomerDTO customerToCustomerDTO(Customer customer);
    Customer customerDTOToCustomer (CustomerDTO customerDTO);
}
